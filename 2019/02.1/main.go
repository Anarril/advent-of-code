package main

import (
	"fmt"
	"syscall"
)

type OpCode struct {
	instruction int
	input0      int
	input1      int
	output      int
}

func main() {
	fmt.Printf("2019.02.0\n")

	for noun := 1; noun <= 99; noun++ {
		for verb := 1; verb <= 99; verb++ {
			program := getProgram()
			runProgram(noun, verb, program)
		}
	}

}

func runProgram(noun int, verb int, opCodes []int) {
	opCodes[1] = noun
	opCodes[2] = verb

	for i := 0; i+4 < len(opCodes); i += 4 {
		fmt.Printf("index: %d\n", i)
		opCode := getNextOpCode(i, opCodes)
		processOpCode(opCode, opCodes)
	}
}

func processOpCode(opCode OpCode, opCodes []int) {
	switch opCode.instruction {
	case 1:
		opCodes[opCode.output] = opCodes[opCode.input0] + opCodes[opCode.input1]
		break
	case 2:
		opCodes[opCode.output] = opCodes[opCode.input0] * opCodes[opCode.input1]
		break
	case 99:
		if opCodes[0] == 19690720 {
			fmt.Printf("SUCCESS: %d, %d = %d\n", opCodes[1], opCodes[2], opCodes[0])
			syscall.Exit(0)
		}
		break
	default:
		syscall.Exit(1)
	}
}

func getNextOpCode(index int, opCodes []int) OpCode {
	return OpCode{
		instruction: opCodes[index],
		input0:      opCodes[index+1],
		input1:      opCodes[index+2],
		output:      opCodes[index+3],
	}
}

func getProgram() []int {
	return []int{
		1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2,
		1, 9, 19, 1, 5, 19, 23, 1, 6, 23, 27, 1, 27, 10, 31, 1, 31, 5, 35, 2, 10, 35, 39,
		1, 9, 39, 43, 1, 43, 5, 47, 1, 47, 6, 51, 2, 51, 6, 55, 1, 13, 55, 59, 2, 6, 59, 63,
		1, 63, 5, 67, 2, 10, 67, 71, 1, 9, 71, 75, 1, 75, 13, 79, 1, 10, 79, 83, 2, 83, 13, 87,
		1, 87, 6, 91, 1, 5, 91, 95, 2, 95, 9, 99, 1, 5, 99, 103, 1, 103, 6, 107, 2, 107, 13, 111,
		1, 111, 10, 115, 2, 10, 115, 119, 1, 9, 119, 123, 1, 123, 9, 127, 1, 13, 127, 131, 2, 10,
		131, 135, 1, 135, 5, 139, 1, 2, 139, 143, 1, 143, 5, 0, 99, 2, 0, 14, 0,
	}
}
