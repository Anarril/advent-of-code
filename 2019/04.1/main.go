package main

import (
	"fmt"
	"syscall"
)

type password [6]int

func (p *password) toInt() int {
	return 100000*p[0] + 10000*p[1] + 1000*p[2] + 100*p[3] + 10*p[4] + 1*p[5]
}

func (p *password) hasDigitsInOrder() bool {
	return p[0] <= p[1] && p[1] <= p[2] && p[2] <= p[3] && p[3] <= p[4] && p[4] <= p[5]
}

func (p *password) hasRepeat() bool {
	return p[0] == p[1] || p[1] == p[2] || p[2] == p[3] || p[3] == p[4] || p[4] == p[5]
}

func (p *password) hasTwoDigitRepeat() bool {
	validRepeat := false
	ignoreValue := -1
	for i := 0; i <= 5; i++ {
		if i > 0 && !validRepeat && p[i-1] == p[i] && p[i] != ignoreValue {
			if i < 5 && p[i] == p[i+1] {
				ignoreValue = p[i]
			} else {
				validRepeat = true
			}
		}
	}

	return validRepeat
}

func (p *password) increment(digit int) {
	if p[digit] == 9 && digit == 0 {
		syscall.Exit(1)
		return
	}

	if p[digit] == 9 {
		p.increment(digit - 1)
		p[digit] = p[digit-1]
		return
	}

	if digit != 0 && p[digit] < p[digit-1] {
		p[digit] = p[digit-1]
		return
	}

	p[digit]++
	return
}

func main() {
	fmt.Printf("2019.04.1\n")

	// minValue := 231832
	passwd := password{
		0: 2,
		1: 3,
		2: 1,
		3: 8,
		4: 3,
		5: 2,
	}

	maxValue := 767346

	counter := 0
	for ; passwd.toInt() < maxValue; passwd.increment(5) {
		if passwd.hasDigitsInOrder() && passwd.hasRepeat() && passwd.hasTwoDigitRepeat() {
			counter++
		}
	}

	fmt.Printf("Result: %d\n\n", counter)
}
