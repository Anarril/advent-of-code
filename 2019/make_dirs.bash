#!/usr/bin/env bash

for i in $(seq -f "%02g" 1 25); do
  for j in {0..1}; do
    mkdir "$i.$j"

    printf "package main

import (
	\"fmt\"
)

func main() {
	fmt.Printf(\"2019.%s.%d\\\n\")
}
" $i $j > "$i.$j/main.go"

  done
done
