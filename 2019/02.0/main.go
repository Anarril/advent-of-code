package main

import (
	"fmt"
	"syscall"
)

type OpCode struct {
	instruction int
	input0      int
	input1      int
	output      int
}

func main() {
	fmt.Printf("2019.02.0\n")
	program := getProgram()
	program[1] = 12
	program[2] = 2

	runProgram(program)

}

func runProgram(opCodes []int) {
	for i := 0; i < len(opCodes); i += 4 {
		opCode := getNextOpCode(i, opCodes)
		fmt.Printf("%d - %d\n", i, opCode.instruction)
		processOpCode(opCode, opCodes)
	}
}

func processOpCode(opCode OpCode, opCodes []int) {
	switch opCode.instruction {
	case 1:
		opCodes[opCode.output] = opCodes[opCode.input0] + opCodes[opCode.input1]
		break
	case 2:
		opCodes[opCode.output] = opCodes[opCode.input0] * opCodes[opCode.input1]
		break
	case 99:
		fmt.Printf("Result: %d\n", opCodes[0])
		syscall.Exit(0)
		break
	default:
		syscall.Exit(1)
	}
}

func getNextOpCode(index int, opCodes []int) OpCode {
	return OpCode{
		instruction: opCodes[index],
		input0:      opCodes[index+1],
		input1:      opCodes[index+2],
		output:      opCodes[index+3],
	}
}

func getProgram() []int {
	return []int{
		1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2,
		1, 9, 19, 1, 5, 19, 23, 1, 6, 23, 27, 1, 27, 10, 31, 1, 31, 5, 35, 2, 10, 35, 39,
		1, 9, 39, 43, 1, 43, 5, 47, 1, 47, 6, 51, 2, 51, 6, 55, 1, 13, 55, 59, 2, 6, 59, 63,
		1, 63, 5, 67, 2, 10, 67, 71, 1, 9, 71, 75, 1, 75, 13, 79, 1, 10, 79, 83, 2, 83, 13, 87,
		1, 87, 6, 91, 1, 5, 91, 95, 2, 95, 9, 99, 1, 5, 99, 103, 1, 103, 6, 107, 2, 107, 13, 111,
		1, 111, 10, 115, 2, 10, 115, 119, 1, 9, 119, 123, 1, 123, 9, 127, 1, 13, 127, 131, 2, 10,
		131, 135, 1, 135, 5, 139, 1, 2, 139, 143, 1, 143, 5, 0, 99, 2, 0, 14, 0,
	}
}
