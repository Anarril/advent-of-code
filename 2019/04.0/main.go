package main

import (
	"fmt"
	"syscall"
)

type password [6]int

func (p *password) toString() string {
	return fmt.Sprintf("%d%d%d%d%d%d", p[0], p[1], p[2], p[3], p[4], p[5])
}

func (p *password) toInt() int {
	return 100000*p[0] + 10000*p[1] + 1000*p[2] + 100*p[3] + 10*p[4] + 1*p[5]
}

func (p *password) hasRepeat() bool {
	return p[0] == p[1] || p[1] == p[2] || p[2] == p[3] || p[3] == p[4] || p[4] == p[5]
}

func (p *password) increment(digit int) {
	if p[digit] == 9 && digit == 0 {
		syscall.Exit(1)
		return
	}

	if p[digit] == 9 {
		p.increment(digit - 1)
		p[digit] = p[digit-1]
		return
	}

	if digit != 0 && p[digit] < p[digit-1] {
		p[digit] = p[digit-1]
		return
	}

	p[digit]++
	return
}

func main() {
	fmt.Printf("2019.04.0\n")

	// minCode := 231832
	// manual adjustment to the first valid code:
	// minCode = 233333

	code := password{
		0: 2,
		1: 3,
		2: 3,
		3: 3,
		4: 3,
		5: 3,
	}

	maxCode := 767346

	counter := 0
	for ; code.toInt() < maxCode; code.increment(5) {
		fmt.Printf("code: %s\n", code.toString())
		if code.hasRepeat() {
			counter++
		}
	}

	fmt.Printf("Result: %d\n\n", counter)
}
